variable "add_to_cart_image_id" {
  type = string
  default = "wi11i4m/add-to-cart"
}

variable "order_service_image_id" {
  type = string
  default = "wi11i4m/order-service"
}