# Terraform Script to deploy DomosShop in Docker local 🤖

- [ ] Download the main.tf file

> Make sure you have [docker](https://docs.docker.com/engine/install/) and [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) installed in your computer

- [ ] Run the command

```sh
terraform init
terraform apply --auto-approve
```

- [ ] Now, All applications are successful deploy in your docker local 🤠

> Visit [Frontend App](http://localhost:4200), [Backend App - Customers](http://localhost:5000/customer), [Backend App - Orders](http://localhost:5000/order)

- [ ]