terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.22.0"
    }
  }
}

provider "docker" {}

resource "docker_network" "pit_network" {
  name   = "domosshop_network"
  driver = "bridge"
}

resource "docker_image" "mongodb" {
  name         = "mongo:focal"
  keep_locally = true
}
resource "docker_container" "mongodb" {
  image = docker_image.mongodb.image_id
  name  = "mongodb"
  ports {
    internal = 27017
    external = 27017
  }
  env = [
    "MONGO_INITDB_ROOT_USERNAME=root",
    "MONGO_INITDB_ROOT_PASSWORD=william"
  ]
  volumes {
    container_path = "/data/db"
    volume_name    = "mongo-data"
  }
  networks_advanced {
    name    = docker_network.pit_network.name
    aliases = ["domosshop"]
  }
}

resource "docker_image" "order_service" {
  name         = var.order_service_image_id
  keep_locally = true
}
resource "docker_container" "order_service" {
  image = docker_image.order_service.image_id
  name  = "order-service"
  ports {
    internal = 5000
    external = 5000
  }
  depends_on = [
    docker_container.mongodb,
    docker_container.kafka
  ]
  networks_advanced {
    name    = docker_network.pit_network.name
    aliases = ["domosshop"]
  }
}

resource "docker_image" "add_to_cart" {
  name         = var.add_to_cart_image_id
  keep_locally = true
}
resource "docker_container" "add_to_cart" {
  image = docker_image.add_to_cart.image_id
  name  = "add-to-cart"
  ports {
    internal = 80
    external = 4200
  }
  networks_advanced {
    name    = docker_network.pit_network.name
    aliases = ["domosshop"]
  }
}

resource "docker_image" "zookeeper" {
  name         = "confluentinc/cp-zookeeper:7.0.0"
  keep_locally = true
}
resource "docker_container" "zookeeper" {
  image = docker_image.zookeeper.image_id
  name  = "zookeeper"
  ports {
    internal = 32181
    external = 32181
  }
  env = [
    "ZOOKEEPER_CLIENT_PORT=32181",
    "ZOOKEEPER_TICK_TIME=2000"
  ]
  volumes {
    container_path = "/var/lib/zookeeper/data"
    volume_name    = "zookeeper-data"
  }
  networks_advanced {
    name    = docker_network.pit_network.name
    aliases = ["domosshop"]
  }
}

resource "docker_image" "kafka" {
  name         = "confluentinc/cp-enterprise-kafka:7.0.0"
  keep_locally = true
}
resource "docker_container" "kafka" {
  image = docker_image.kafka.image_id
  name  = "kafka"
  ports {
    internal = 29092
    external = 29092
  }
  depends_on = [
    docker_container.zookeeper
  ]
  env = [
      "KAFKA_BROKER_ID=1",
      "KAFKA_ZOOKEEPER_CONNECT=zookeeper:32181",
      "KAFKA_LISTENER_SECURITY_PROTOCOL_MAP=PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT",
      "KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1",
      "KAFKA_INTER_BROKER_LISTENER_NAME=PLAINTEXT",
      "KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://kafka:29092,PLAINTEXT_HOST://localhost:9092",
      "KAFKA_AUTO_CREATE_TOPICS_ENABLE=false",
      "KAFKA_METRIC_REPORTERS: io.confluent.metrics.reporter.ConfluentMetricsReporter",
      "CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS=kafka:29092",
      "CONFLUENT_METRICS_REPORTER_ZOOKEEPER_CONNECT=zookeeper:32181",
      "CONFLUENT_METRICS_REPORTER_TOPIC_REPLICAS=1",
      "CONFLUENT_METRICS_ENABLE='true'",
      "CONFLUENT_SUPPORT_CUSTOMER_ID='anonymous'"
  ]
  volumes {
    container_path = "/var/lib/kafka/data"
    volume_name    = "kafka-data"
  }
  networks_advanced {
    name    = docker_network.pit_network.name
    aliases = ["domosshop"]
  }
}
